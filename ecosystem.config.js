module.exports = {
  /**
   * Deployment section
   * http://pm2.keymetrics.io/docs/usage/deployment/
   */
  deploy : {
    prod : {
      key  : '~/.ssh/node_id_rsa',
      user : 's.grishanovich',
      host : '192.168.5.227',
      ref  : 'origin/master',
      repo : 'https://s_grishanovich@bitbucket.org/s_grishanovich/deploy_test.git',
      path : '/home/s.grishanovich/deploy_test_dir',
      'post-deploy' : 'npm install && pm2 startOrRestart ecosystem.config.js --env production'
    }
  }
};
