'use strict';

let id = 0;
let timerId = setInterval(() => {
    console.log('Tick ' + id++);
}, 1000);

process.on('SIGINT', () => {
    console.warn('Received SIGINT');
    clearInterval(timerId);
});
